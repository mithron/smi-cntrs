import requests
import io,os,logging
import argparse
import json
import csv
from tqdm import tqdm, trange
from datetime import datetime, timedelta

def init():
    pass

def get_data_by_okdp():
    data = []
    req = {"okdp_okpd":"72.22.12"}
    resp = requests.get("http://openapi.clearspending.ru/restapi/v3/contracts/search/", params = req)
    respdata = resp.json()['contracts']
    data = respdata['data']
    for i in range(1, int(respdata['total'])+1):
        req['page']=i
        resp = requests.get("http://openapi.clearspending.ru/restapi/v3/contracts/search/", params = req)
        respdata = resp.json()['contracts']
        data = data + respdata['data']
    return data

def get_contr_by_date(keywords = ["сайт", 'www'], start_date = "01.01.2011"):
    st_date = datetime.strptime(start_date, "%d.%m.%Y").date()
    dates = []
    end_date = st_date +timedelta(days = 90)
    while end_date < datetime.now().date():
        dates.append(end_date)
        end_date = end_date +timedelta(days = 90)
    dates.append(datetime.now().date())
    data = []
    for end_date in tqdm(dates):
        for word in tqdm(keywords):
            req = {"productsearch": '"'+word+'"', "daterange": st_date.strftime('%d.%m.%Y')+'-'+ end_date.strftime('%d.%m.%Y')}
            resp = requests.get("http://openapi.clearspending.ru/restapi/v3/contracts/search/", params=req)
            if resp.ok:
                respdata = resp.json()['contracts']
                data = data + respdata['data']
            else:
                print(resp)
        st_date = end_date
    return data        
    
def get_filtered_data(keywords = ["сайт", 'www'], start_date = "01.01.2011"):
    st_date = datetime.strptime(start_date, "%d.%m.%Y").date()
    dates = []
    end_date = st_date +timedelta(days = 90)
    while end_date < datetime.now().date():
        dates.append(end_date)
        end_date = end_date +timedelta(days = 90)
    dates.append(datetime.now().date())
    data = []
    for end_date in tqdm(dates):
        for word in tqdm(keywords):
            req = {"productsearch": '"'+word+'"', "daterange": st_date.strftime('%d.%m.%Y')+'-'+ end_date.strftime('%d.%m.%Y')}
            resp = requests.get("http://openapi.clearspending.ru/restapi/v3/contracts/search/", params=req)
            if resp.ok:
                respdata = resp.json()['contracts']
                data = data + filter_fields(respdata['data'])
            else:
                print(resp)
        st_date = end_date
    return data

def write_json(filename, data):
    with io.open(filename, 'w') as jsonfile:
        json.dump(data, jsonfile)

def get_value():
    pass

def filter_fields(data):
    newdata = []
    print('Filtering out')
    for item in tqdm(data):
        try:
            for i in trange(len(item['products'])):
                newitem = {}
                newitem['regNum'] = item.get('regNum',"")
                try:
                    newitem['name'] = item['economic_sectors'][0].get('title', "")
                except:
                    newitem['name'] = ""
                try:
                    newitem['okei_code'] = item['products'][i]['OKEI'].get('code',"")
                except:
                    newitem['okei_code'] = ""
                try:
                    newitem['okpd_code'] = item['products'][i]['OKDP'].get('code',"")
                except:
                    newitem['okpd_code'] = ""
                try:
                    newitem['product_name'] = item['products'][i].get('name',"")
                except:
                    newitem['product_name'] = ""
                try:
                    newitem['price'] = item['products'][i].get('price',0)
                except:
                    newitem['price'] = ""
                try:
                    newitem['quantity'] = item['products'][i].get('quantity',1)
                except:
                    newitem['quantity'] = 1
                try:
                    newitem['sum'] = item['products'][i].get('sum',0)
                except:
                    newitem['sum'] = ""
        
                newitem['region'] = item.get('regionCode',"")
        
                newitem['currentcontractstage'] = item.get('currentContractStage',"")
        
                try:
                    newitem['currencycode'] = item['currency']['code']
                except:
                    newitem['currencycode'] = ""
            
                newitem['publishdate'] = item.get('publishDate',"")
            
                newdata.append(newitem)
        except:
            pass
    return newdata

def main(filename = "results.json", loglevel = logging.ERROR, source = 'Source.csv'):
    with io.open(source, 'r', encoding="windows-1251") as keyfile:
        keywords = []
        keyreader = csv.reader(keyfile)
        for row in keyreader:
            try:
                keywords.append(row[0])
            except:
                continue
        data = get_contr_by_date(keywords)
        with io.open(filename, 'w') as jsonfile:
            json.dump(data, jsonfile)
        newdata = filter_fields(data)
        fieldnames  =  ['regNum','name','okei_code','okpd_code','product_name','price','quantity','sum','region',
                    'currentcontractstage','currencycode','publishdate']
        with io.open('results.csv', 'w') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames)
            writer.writerow(fieldnames)
            for item in tqdm(newdata):
                writer.writerow(item)

def main2(filename = "results2.json", loglevel = logging.ERROR, source = 'Source.csv'):
    with io.open(source, 'r', encoding="windows-1251") as keyfile:
        keywords = []
        keyreader = csv.reader(keyfile)
        for row in keyreader:
            try:
                keywords.append(row[0])
            except:
                continue
        data = get_filtered_data(keywords)
        with io.open(filename, 'w') as jsonfile:
            json.dump(data, jsonfile)
        fieldnames  =  ['regNum','name','okei_code','okpd_code','product_name','price','quantity','sum','region',
                    'currentcontractstage','currencycode','publishdate']
        with io.open('results2.csv', 'w') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames)
            writer.writerow(fieldnames)
            for item in tqdm(data):
                writer.writerow(item)
                
                
def csv_to_json(csvfile = 'web/filtered.csv', jsonfile = 'web/filterd.json'):
    with io.open(csvfile, 'r') as csvtojs:
        csvreader = csv.DictReader(csvtojs)
        data = []
        for row in csvreader:
            try:
                item = {}
                item['regNum'] = row['regNum']
                item['sum'] = row['sum']
                item['quantity'] = row.get('quantity',1)
                item['price'] = row.get('price', 0)
                item['regioncode'] = row['region']
                item['date'] = row['date'].split('T')[0]
                data.append(item)
            except:
                pass
        with io.open(jsonfile, 'w') as json_file:
            json.dump(data, json_file)
 
def csv_to_json2(csvfile = 'web/filtered.csv', jsonfile = 'web/filterd.json'):
    with io.open(csvfile, 'r') as csvtojs:
        csvreader = csv.DictReader(csvtojs)
        data = []
        for row in csvreader:
            try:
                data.append(row)
            except:
                pass
        with io.open(jsonfile, 'w') as json_file:
            json.dump(data, json_file)
 

 
def geojson_antimeridian(geojsonfile = 'web/regions.geojson'):
    with io.open(geojsonfile, 'r') as data:
        geodata = json.load(data)
        for region in geodata['features']:
            try:
                for coors in region['geometry']['coordinates']:
                    for coods in coors:
                        if coods[0] < 0:
                            coods[0] = coods[0] + 360
                            coods.append('true')
            except:
                pass
        with io.open("regions_fixed.geojson", "w") as newfile:
            json.dump(geodata, newfile)

def split_coords(jsonfile, new):
    with io.open(jsonfile, 'r') as data:
        geodata = json.load(data)
        for item in geodata:
            item['lat'] = item['coords'].split(',')[0]
            item['lng'] = item['coords'].split(',')[1]
        with io.open(new, "w") as newfile:
            json.dump(geodata, newfile)
            
        
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='clearspending scrapper')
    parser.add_argument('-f', '--filename',  dest="filename", action='store', default = 'fih_results.csv',
                   help='CSV filename to store results. Default results.json')
    parser.add_argument('-s', dest = 'source', action='store', default = 'Source.csv', help = 'фаил с ключевые слова')
    parser.add_argument('-l', '--loglevel', dest='loglevel', action='store', default = logging.ERROR, type=int,
                   help='Logging Level. 10 to 50, the lower is more verbose.')
    args = parser.parse_args()
    
    
    
    main(filename = args.filename, source = args.source, loglevel = args.loglevel )   
    
